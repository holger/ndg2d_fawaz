#include <iostream>

template<int n1, int n2>
class Array2d
{
public:
  Array2d() {};

  int dim1() {
    return n1;
  }

  int dim2() {
    return n2;
  }

  double& operator() (int i, int j) {
    if(i < 0 || i >= n1 | j < 0 || j >= n2) {
      std::cout << "ERROR\n";
      exit(1);
    }
    return data[i][j];
  }

private:

  double data[n1][n2];  
};

int main()
{
  std::cout << "Hello Mohamad!\n";

  Array2d<10,2> array;

  for(int i=0; i<array.dim1(); i++) {
    array(i,0) = 2;
    array(i,1) = 0;
  }

  Array2d<10,2> array2;

  array2 = array;

  for(int i=0; i<array.dim1(); i++) {
    std::cout << array(i,0) << "\t" << array(i,1) << "\t" << array2(i,0) << "\t" << array2(i,1) << std::endl;
  }
  
  array(1,1) = 5;
  array2(1,1) = 10;

  for(int i=0; i<array.dim1(); i++) {
    std::cout << array(i,0) << "\t" << array(i,1) << "\t" << array2(i,0) << "\t" << array2(i,1) << std::endl;
  }

  std::cout << "This is the end\n";

}
