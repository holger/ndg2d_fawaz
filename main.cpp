#include <iostream>
#include <math.h>
#include <fstream>
#include <vector>
#include <array>

using namespace std;

void write(std::string filename, std::vector<std::array<double,3>> C, int n, double deltaX) {

  std::vector<double> Uexact(n);
  std::vector<double> U(n);
  
  std::ofstream out(filename.c_str());
  for (int i = 0; i < n; i++) {
    for (int j = 0; j <= 50; j++)
    {
      double t = (-1 + (double(j) / 50) * 2);
      Uexact[i] = sin(t);// 
      U[i] = (C[i][0] / 2) * (t * t - t) + C[i][1] * (1 - t * t) + (C[i][2] / 2) * (t * t + t);
      out << i * 50 + j << "\t" << U[i] << std::endl;
      // out << i*50+j << "\t" <<  Uexact[i]<<   std::endl;
    }
  }

  out.close();
}

double flux_numerique()
{}

double integralGauss()
{}

  

int main()
{
  int n = 16 ;
  
  double P = M_PI;
  double v = 2;
  double vv = 4*v; // multiple of 4

  double lambda = 2*P;

  double f = 2;

  double deltaX = lambda / (n-2);

  double L = 2*P+2*deltaX;
  
  double T = 5;
  double deltaT = 0.1; // TIME STEP
  double a = 0.02;
  double CFL = deltaT / deltaX;
  double b = a * CFL ;

  int m = T / deltaT;
  
  std::vector<std::array<double,3>> C(n);

  for (int i = 0; i < n; i++) {
    C[i][0] = sin(i * deltaX);
    C[i][1] = sin((i + 0.5) * deltaX);
    C[i][2] = sin((i + 1) * deltaX);
  }
  
  write("initial.txt", C, n, deltaX);

  double C1[n][3]; // K1
  double C2[n][3];
  double C3[n][3];
  double C4[n][3];

  double C1m[n][3]; // K1 modify 
  double C2m[n][3];
  double C3m[n][3];
  double C4m[n][3];

  for (int s = 1; s <= 10; s++)
  {
    for (int i = 1; i < n-1; i++)
    {
      C1[i][0] = b * (3 * C[i-1][2] -4*C[i][1] + C[i][2]);
      C1[i][1] = b * (C[i-1][2] - C[i][2]);
      C1[i][2] = b * (-7*C[i-1][2] +4*C[i][1] + 3*C[i][2]);

      C1m[i][0] = C[i][0] + C1[i][0]/2;
      C1m[i][1] = C[i][1] + C1[i][1]/2;
      C1m[i][2] = C[i][2] + C1[i][2]/2;

      C2[i][0] = b * (3 * C1m[i][0] -4*C1m[i][1] + C1m[i][2]);
      C2[i][1] = b * (C1m[i][0] - C1m[i][2]);
      C2[i][2] = b * (-7*C1m[i][0] +4*C1m[i][1] + 3*C1m[i][2]);
      C2m[i][0] = C[i][0] + C2[i][0]/2;
      C2m[i][1] = C[i][1] + C2[i][1]/2;
      C2m[i][2] = C[i][2] + C2[i][2]/2;

      C3[i][0] = b * (3 * C2m[i][0] -4*C2m[i][1] + C2m[i][2]);
      C3[i][1] = b * (C2m[i][0] - C2m[i][2]);
      C3[i][2] = b * (-7*C2m[i][0] +4*C2m[i][1] + 3*C2m[i][2]);
      C3m[i][0] = C[i][0] + C3[i][0];
      C3m[i][1] = C[i][1] + C3[i][1];
      C3m[i][2] = C[i][2] + C3[i][2];

      C4[i][0] = b * (3 * C3m[i][0] -4*C3m[i][1] + C3m[i][2]);
      C4[i][1] = b * (C3m[i][0] - C3m[i][2]);
      C4[i][2] = b * (-7*C3m[i][0] +4*C3m[i][1] + 3*C3m[i][2]);
      
      C[i][0] = C[i][0]  + (C1[i][0] + 2*C2[i][0] + 2*C3[i][0] + C4[i][0])/6; // The End Resukts 
      C[i][1] = C[i][1]  + (C1[i][1] + 2*C2[i][1] + 2*C3[i][1] + C4[i][1])/6;
      C[i][2] = C[i][2]  + (C1[i][2] + 2*C2[i][2] + 2*C3[i][2] + C4[i][2])/6;
      
    /*for (int j = 0; j <= 50; j++)
    {
      double jj = j;
      double t = (-1 + (jj / 50) * 2);
      Uexact[i] = sin(t);
      U[i] = (C[i][0] / 2) * (t * t - t) + C[i][1] * (1 - t * t) + (C[i][2] / 2) * (t * t + t);
      out << i * 50 + j << "\t" << U[i] << std::endl;
      // out << i*50+j << "\t" <<  Uexact[i]<<   std::endl;
    }
*/

    }

    C[0][0] = C[n-2][0];
    C[0][1] = C[n-2][1];
    C[0][2] = C[n-2][2];

    C[n-1][0] = C[1][0];
    C[n-1][1] = C[1][1];
    C[n-1][2] = C[1][2];

    // out.close(); 

  }

  write("final.txt", C, n, deltaX);
  
  return 0;
}
